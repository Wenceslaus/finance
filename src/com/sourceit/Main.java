package com.sourceit;

import com.sourceit.lecture.Bank;
import com.sourceit.lecture.BlackMarket;
import com.sourceit.lecture.Exchange;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Object[] exchanges = new Object[2];

        exchanges[0] = new Bank("Privat", 27.3);
        exchanges[1] = new BlackMarket(27.2, "Roza");


        try {
            Object bestExchange = exchanges[0];
            double maxUsd = ((Exchange) bestExchange).convert(15_000);
            for (Object exchange : exchanges) {

                double currentUsd = ((Exchange) bestExchange).convert(15_000);

                if (maxUsd < currentUsd) {
                    bestExchange = exchange;
                    maxUsd = currentUsd;
                }

            }

            if (bestExchange instanceof Bank) {
                Bank bank = (Bank) bestExchange;
                System.out.println(bank.name + ": " + maxUsd);
            } else {
                BlackMarket blackMarket = (BlackMarket) bestExchange;
                System.out.println(blackMarket.name + ": " + maxUsd);
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Sorry");
        } catch (IOException e) {
            System.out.println("Sorry, ----");
        }


    }
}
