package com.sourceit.lecture;

import java.io.IOException;

/**
 * Created by wenceslaus on 21.03.17.
 */
public class Bank implements Exchange, Kredit {

    double usd;
    public String name;

    public Bank(String name, double usd) {
        this.usd = usd;
        this.name = name;
    }

    @Override
    public double convert(int uan) throws IOException {
        if (uan > 12_000) {
            throw new IOException("Must be less then 12000 uan");
        }
        return (uan - 15) / usd;
    }

    @Override
    public double getKredit() {
        return 200_000;
    }
}
