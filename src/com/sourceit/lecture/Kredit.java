package com.sourceit.lecture;

/**
 * Created by wenceslaus on 21.03.17.
 */
public interface Kredit {

    double getKredit();
}
