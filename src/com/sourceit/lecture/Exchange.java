package com.sourceit.lecture;

import java.io.IOException;

/**
 * Created by wenceslaus on 21.03.17.
 */
public interface Exchange {

    double convert(int uan) throws IOException;
}
