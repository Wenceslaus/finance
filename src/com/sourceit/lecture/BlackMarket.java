package com.sourceit.lecture;

/**
 * Created by wenceslaus on 21.03.17.
 */
public class BlackMarket implements Exchange {

    double usd;
    public String name;

    public BlackMarket(double usd, String name) {
        this.usd = usd;
        this.name = name;
    }

    @Override
    public double convert(int uan) {
        return uan / usd;
    }
}
